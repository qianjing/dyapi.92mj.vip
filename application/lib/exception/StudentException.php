<?php


namespace app\lib\exception;


class StudentException extends BaseException
{
    public $code = 400;
	public $msg = '失败';
	public $errorCode = 10000;
}