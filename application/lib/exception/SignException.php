<?php


namespace app\lib\exception;


class SignException extends BaseException
{
    public $code = 400;
	public $msg = '已签到';
	public $errorCode = 10000;
}