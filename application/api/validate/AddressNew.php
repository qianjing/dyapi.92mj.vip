<?php 

namespace app\api\validate;

class AddressNew extends BaseValidate
{
	protected $rule = [
		'name' => 'require|isNotEmpty',
		'gender' => 'require',
		'mobile' => 'require|isNotEmpty',
		'address' => 'require|isNotEmpty',
		'detail' => 'require|isNotEmpty'
	];
}