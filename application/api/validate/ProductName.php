<?php 

namespace app\api\validate;


class ProductName extends BaseValidate
{
	protected $rule = [
		'name' => 'require|isNotEmpty',
	];

	protected $message = [
		'name' => '搜索内容不能为空'
	];
}