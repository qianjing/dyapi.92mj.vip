<?php


namespace app\api\validate;


class TokenInviteGet extends BaseValidate
{
    protected $rule = [
        'code' => 'require|isNotEmpty',
        'head_id'   => 'require|isNotEmpty',
        'type'  => 'require|isNotEmpty'
    ];
}