<?php 

namespace app\api\validate;

/**
 * 
 */
class UserPhone extends BaseValidate
{
	
	protected $rule = [
		'sessionKey' => 'require|isNotEmpty',
		'encryptedData' => 'require|isNotEmpty',
		'iv' => 'require|isNotEmpty',
	];
}