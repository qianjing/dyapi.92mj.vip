<?php


namespace app\api\validate;


class Account extends BaseValidate
{
    protected $rule = [
        'cardholder' => 'require|isNotEmpty',
        'cardnumber' => 'require|isNotEmpty',
        'bank' => 'require|isNotEmpty',
    ];
}