<?php


namespace app\api\validate;


class StudentPass extends BaseValidate
{
    protected $rule = [
        'old' => 'require|isNotEmpty',
        'new_password' => 'require|isNotEmpty'
    ];

    protected $message = [
        'old' => '原密码不能为空',
        'new_password' => '新密码不能为空',
    ];
}