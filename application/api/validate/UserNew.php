<?php


namespace app\api\validate;


class UserNew extends BaseValidate
{
    protected $rule = [
        'nickname' => 'require|isNotEmpty',
        'mobile' => 'require|isMobile',
        'gender' => 'require|in:0,1',
        'birthday' => 'require|isNotEmpty'
    ];

    protected $message = [
        'nickname' => '昵称不能为空',
        'mobile' => '手机号格式不正确',
        'gender' => '性别必须为0或1',
        'birthday' => '生日不能为空'
    ];
}