<?php


namespace app\api\model;


class Studio extends BaseModel
{
    protected $hidden = ['create_time', 'update_time', 'delete_time', 'douyincode'];

    protected function base($query)
    {
        $query->order('createtime desc');
    }

    public function getStudioImageAttr($value, $data)
    {
        $findUrl = config('setting.img_prefix').$value;

        return $findUrl;
    }

    public function getDetailImageAttr($value, $data)
    {
        $findUrl = config('setting.img_prefix').$value;

        return $findUrl;
    }

    public static function allStu($dealer_id)
    {
        $data = self::where('FIND_IN_SET(:id,dealer_ids)',['id' => $dealer_id])
            ->field('id, name, hostname, studioimage, desc')
            ->select();

        return $data;
    }

    public static function getOne($id)
    {
        $one = self::field('id, name, desc, hostname, studioimage, detailimage, link')
            ->where('id','=', $id)
            ->find();

        return $one;
    }
}