<?php

namespace app\api\controller;

use app\lib\exception\StudentException;
use think\Cache;
use think\Controller;
use app\api\service\Token as TokenService;

class BaseController extends Controller
{
	protected function checkPrimaryScope()
	{
		TokenService::needPrimaryScope();
	}

	public function checkExclusiveScope()
	{
		TokenService::needExclusiveScope();
	}

	// 检查商学院用户有没有登录
	public function checkLogin()
    {
        $userinfo = Cache::get('student');

        if (!$userinfo){
            throw new StudentException([
                'msg' => '请重新登录'
            ]);
        }
    }
}