<?php 

namespace app\api\controller\v1;

use app\api\model\WXBizDataCrypt;
use app\api\service\AppToken;
use app\api\service\ConsumerToken;
use app\api\validate\AppTokenGet;
use app\api\validate\TokenGet;
use app\api\service\UserToken;
use app\api\validate\TokenInviteGet;
use app\lib\exception\ParameterException;
use app\api\service\Token as TokenService;
use app\api\validate\UserPhone;

class Token
{
    // 获取token
	public function getToken($code = '')
	{
		(new TokenGet())->goCheck();
		$ut = new UserToken($code);
		$result = $ut->get();

		// return [
		// 	'token' => $token
		// ];

		return $result;
	}

	// 邀请用户的token
    public function inviteToken($code = '', $head_id = '', $type = '')
    {
        (new TokenInviteGet())->goCheck();

        $ut = new UserToken($code);
        $result = $ut->get($head_id, $type);

    }

	// 校验token
	public function verifyToken($token='')
	{
		if(!$token){
			throw new ParameterException([
				'msg'=>'token不允许为空'
			]);
		}
		$valid = TokenService::verifyToken($token);
		$result = [];

		if($valid){
            $result['code'] = 1;
            $result['msg'] = 'success';
            $result['data'] = '';
        }else{
            $result['code'] = 0;
            $result['msg'] = 'error';
            $result['data'] = '';
        }

		return $result;
	}

	/*
	 * 第三方应用获取令牌(cms)
	 * @url /app_token?
	 * @POST ac=:ac se=:secret
	 */
	public function getAppToken($ac='', $se='')
	{
		(new AppTokenGet())->goCheck();

		$app = new AppToken();
		$token = $app->get($ac, $se);

		return [
			'token' => $token
		];
	}

	/*
		解密用户手机号等信息
	*/
	public function getUserPhone()
	{
        $validate = new UserPhone();
        $validate->gocheck();

        $WxData = $validate->getDataByRule(input('post.'));

        $pc = new WXBizDataCrypt(config('wx.app_id'), $WxData["sessionKey"]);
        $errCode = $pc->decryptData($WxData["encryptedData"], $WxData["iv"], $data );

        if ($errCode == 0) {
            $userinfo = json_decode($data);
            $result = ConsumerToken::get($userinfo);

            return $result;
        } else {
            return $errCode;
        }
	}
}